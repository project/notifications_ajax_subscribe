Notifications AJAX Subscribe: README.txt
=========================

Provides checkbox subscription display options for the Notifications module.
The checkboxes are processed via AJAX, with the subscription status being 
updated on (un)checking the checkbox. The default subscription options are 
always used.

The checkboxes can be added to nodes in the node links section or in a block 
when a single node is being viewed.

If Javascript is not enabled then regular subscription links will be displayed.

Dependencies:
- Notifications module, http://drupal.org/project/notifications

Developers:
-----------
- Oliver Coleman
