/**
 * Attaches the notifications ajax subscribe behaviour to all required fields
 */
Drupal.behaviors.notificationsAJAXSub = function (context) {
  $('a.ajax-subscribe:not(.ajax-subscribe-processed)', context).each(function () {
    //extract fragment of URI containing data we need
    var data = Drupal.notificationsAJAXSub.processURI($(this).attr('href'));
    
    //create checkbox and label, replace link with them
    var checkbox = $('<input type="checkbox" class="saveguard-ignore" />');
    var wrapper = $('<span class="ajax-subscribe"></span>').append(checkbox).append('<span class="label">' + $(this).text() + '</span>');
    $(this).replaceWith(wrapper);
    
    //create object that manages ajax calls when user clicks checkbox
    new Drupal.notificationsAJAXSub(wrapper, checkbox, data);
    
    $(this).addClass('ajax-subscribe-processed');
  });
};


/**
 * A Notifications AJAX Subscribe object.
 */
Drupal.notificationsAJAXSub = function (wrapper, checkbox, data) {
  var nas = this;
  this.data = data; //fragment of URI containing data we need
  this.wrapper = wrapper;
  this.checkbox = checkbox;
  this.resetCheckbox(); //initialise checkbox state
  
  // Use the click event as the change event is broken in IE
  // (And we can be pretty sure the checkbox value changed on a click...)
  this.checkbox.click(function (event) { 
    return nas.change(this, event); 
  });
};


/**
 * Extracts fragment of URI containing subscription data.
 * The server side script will decode this fragment.
 */
Drupal.notificationsAJAXSub.processURI = function (uri) {
  var startIndex = uri.indexOf('notifications/');
  var endIndex = uri.indexOf('?');
  if (endIndex == -1)
    endIndex = uri.length;
  return uri.substring(startIndex+14, endIndex);
}


/**
 * Reset checkbox to whatever our stored data says it should be.
 * This is useful when the AJAX request failed.
 */
Drupal.notificationsAJAXSub.prototype.resetCheckbox = function () {
  this.checkbox.attr('checked', this.data.substring(0, 9) != 'subscribe');
}


/**
 * Set state of throbber. We can't use Drupal's predefined classes for the 
 * throbber on an input because in most browsers the background* setting 
 * of checkbox inputs is ignored.
 */
Drupal.notificationsAJAXSub.prototype.setThrobber = function (state) {
  if (state == true) {
    var throbberURL = Drupal.settings.basePath + 'misc/throbber.gif';
    this.wrapper.attr('style', "background: transparent url(" + throbberURL + ") no-repeat 1.5em -1.25em;");
    $('.label', this.wrapper).attr('style', "padding-left: 1.3em;");
  }
  else {
    this.wrapper.removeAttr('style');
    $('.label', this.wrapper).removeAttr('style');
  }
}


/**
 * Handler for the "change" event on a checkbox.
 */
Drupal.notificationsAJAXSub.prototype.change = function (input, e) {
  var nas = this;
  nas.checkbox.attr('disabled', "disabled");
  nas.setThrobber(true);
  
  $.ajax({
    url: Drupal.settings.basePath + 'notifications_ajax_subscribe',
    type: 'POST',
    data: { nasdata: nas.data },
    dataType: 'json',
    success: function(data, textStatus, jqXHR) {
      if (typeof (data.error) != 'undefined') {
        alert(data.error);
        nas.resetCheckbox();
      }
      else {
        //server will send back new data in the form of a (un)subscription link URI
        //(this data will then be sent to server on the next checkbox (un)checking)
        nas.data = Drupal.notificationsAJAXSub.processURI(data.uri);
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      alert(Drupal.t("An error occurred while updating the subscription status, please contact an administrator to resolve this issue. Error: ") + errorThrown);
      nas.resetCheckbox();
    },
    complete: function() {
      nas.setThrobber(false);
      nas.checkbox.removeAttr('disabled');
    }
  });
};
